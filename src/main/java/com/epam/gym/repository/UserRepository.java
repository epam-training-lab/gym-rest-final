package com.epam.gym.repository;

import com.epam.gym.entity.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    boolean existsByUsernameAndPassword(String username, String password);
    int countByUsernameStartingWith(String username);
    Optional<User> findByUsernameIgnoreCase(String username);

    @Modifying
    @Query(value = "UPDATE app_user u SET u.password = :newPassword WHERE u.username = :username", 
            nativeQuery = true)
    void changePassword(@Param("username") String username, @Param("newPassword") String newPassword);

}
