package com.epam.gym.repository;

import com.epam.gym.entity.Training;
import com.epam.gym.entity.TrainingTypeEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainingRepository extends CrudRepository<Training, Long> {

}
