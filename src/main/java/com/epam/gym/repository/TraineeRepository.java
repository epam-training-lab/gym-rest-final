package com.epam.gym.repository;

import com.epam.gym.entity.Trainee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TraineeRepository extends CrudRepository<Trainee, Long> {

    Optional<Trainee> findByUsernameIgnoreCase(String username);

}
