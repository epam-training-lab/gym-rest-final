package com.epam.gym.mapper;

import com.epam.gym.dto.response.TraineeTrainingDTO;
import com.epam.gym.dto.response.TrainerTrainingDTO;
import com.epam.gym.entity.Training;

import java.util.function.Function;

public class TrainingFindMapper {

    public static Function<Training, TraineeTrainingDTO> mapTrainee = training ->
            new TraineeTrainingDTO(
                    training.getName(),
                    training.getDate(),
                    training.getType().getName(),
                    training.getDuration(),
                    training.getTrainer().getFirstName());

    public static Function<Training, TrainerTrainingDTO> mapTrainer = training ->
            new TrainerTrainingDTO(
                    training.getName(),
                    training.getDate(),
                    training.getType().getName(),
                    training.getDuration(),
                    training.getTrainee().getFirstName());

}
