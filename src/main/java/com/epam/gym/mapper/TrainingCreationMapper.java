package com.epam.gym.mapper;

import com.epam.gym.dto.request.TrainingCreationDTO;
import com.epam.gym.entity.Training;
import com.epam.gym.repository.TraineeRepository;
import com.epam.gym.repository.TrainerRepository;
import com.epam.gym.service.TrainingTypeService;
import org.springframework.stereotype.Component;

@Component
public class TrainingCreationMapper {

    private final TrainingTypeService trainingTypeService;
    private final TraineeRepository traineeRepository;
    private final TrainerRepository trainerRepository;

    public TrainingCreationMapper(TrainingTypeService trainingTypeService,
            TraineeRepository traineeRepository, TrainerRepository trainerRepository) {
        this.trainingTypeService = trainingTypeService;
        this.traineeRepository = traineeRepository;
        this.trainerRepository = trainerRepository;
    }

    public Training mapToTraining(TrainingCreationDTO trainingCreationDTO) {
        return new Training(
                traineeRepository.findByUsernameIgnoreCase(trainingCreationDTO.traineeUsername()).orElseThrow(),
                trainerRepository.findByUsernameIgnoreCase(trainingCreationDTO.trainerUsername()).orElseThrow(),
                trainingCreationDTO.trainingName(),
                trainingTypeService.getTrainingType(trainingCreationDTO.type()),
                trainingCreationDTO.date(),
                trainingCreationDTO.duration());
    }

}
