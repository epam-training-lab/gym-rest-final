package com.epam.gym.mapper;

import com.epam.gym.dto.response.TrainerProfileDTO;
import com.epam.gym.entity.Trainer;

import java.util.List;

public class TrainerProfileMapper {

    public static TrainerProfileDTO trainerToTrainerProfileDTO(Trainer trainer) {
        return new TrainerProfileDTO(
                trainer.getFirstName(),
                trainer.getLastName(),
                trainer.getSpecialization().getName(),
                trainer.isActive(),
                trainer.getTrainees() == null ? List.of() :
                        trainer.getTrainees().stream().map(TraineeListItemMapper.map).toList());
    }

}
