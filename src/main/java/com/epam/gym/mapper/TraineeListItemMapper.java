package com.epam.gym.mapper;

import com.epam.gym.dto.response.TraineeListItemDTO;
import com.epam.gym.entity.Trainee;

import java.util.function.Function;

public class TraineeListItemMapper {
    public static final Function<Trainee, TraineeListItemDTO> map =
            trainer -> new TraineeListItemDTO(
                    trainer.getUsername(),
                    trainer.getFirstName(),
                    trainer.getLastName());

}
