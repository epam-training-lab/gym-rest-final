package com.epam.gym.mapper;

import com.epam.gym.dto.response.TrainingTypeDTO;
import com.epam.gym.entity.TrainingType;

import java.util.function.Function;

public class TrainingTypeMapper {

    public static Function<TrainingType, TrainingTypeDTO> mapToDTO =
        trainingType -> new TrainingTypeDTO(
                trainingType.getId(),
                trainingType.getName());

}
