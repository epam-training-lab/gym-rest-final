package com.epam.gym.mapper;

import java.util.function.Function;

import com.epam.gym.dto.response.TrainerListItemDTO;
import com.epam.gym.entity.Trainer;

public class TrainerListItemMapper {

    public static Function<Trainer, TrainerListItemDTO> map =
            trainer -> new TrainerListItemDTO(
                    trainer.getUsername(),
                    trainer.getFirstName(),
                    trainer.getLastName(),
                    trainer.getSpecialization().getName());
    
}
