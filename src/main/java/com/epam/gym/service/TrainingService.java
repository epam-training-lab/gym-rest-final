package com.epam.gym.service;

import com.epam.gym.dto.request.SecureTrainingCreationDTO;
import com.epam.gym.mapper.TrainingCreationMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.Trainer;
import com.epam.gym.entity.Training;
import com.epam.gym.exception.EntityNotFoundException;
import com.epam.gym.repository.TraineeRepository;
import com.epam.gym.repository.TrainerRepository;
import com.epam.gym.repository.TrainingRepository;

import java.util.List;

@Service
@Slf4j
public class TrainingService {

    private final TrainingRepository trainingRepository;
    private final TraineeRepository traineeRepository;
    private final TrainerRepository trainerRepository;
    private final TrainingCreationMapper trainingCreationMapper;

    @Autowired
    public TrainingService(TrainingRepository trainingRepository,
            TrainingCreationMapper trainingCreationMapper,
            TraineeRepository traineeRepository, TrainerRepository trainerRepository) {
        this.trainingRepository = trainingRepository;
        this.traineeRepository = traineeRepository;
        this.trainerRepository = trainerRepository;
        this.trainingCreationMapper = trainingCreationMapper;
    }

    @Transactional
    public void create(SecureTrainingCreationDTO secureTrainingCreationDTO) {
        Training training = trainingCreationMapper.mapToTraining(secureTrainingCreationDTO.training());

        Trainee trainee = traineeRepository.findByUsernameIgnoreCase(
            secureTrainingCreationDTO.training().traineeUsername())
            .orElseThrow(() -> new EntityNotFoundException("Trainee not found"));

        Trainer trainer = trainerRepository.findByUsernameIgnoreCase(
            secureTrainingCreationDTO.training().trainerUsername())
            .orElseThrow(() -> new EntityNotFoundException("Trainee not found"));
        
        trainee.getTrainers().add(trainer);
        trainer.getTrainees().add(trainee);

        log.info("New training created: ID={}", training.getId());
        trainingRepository.save(training);
    }

    void deleteAll(List<Training> trainings) {
        if (trainings != null) {
            trainingRepository.deleteAll(trainings);
        }
    }

}
