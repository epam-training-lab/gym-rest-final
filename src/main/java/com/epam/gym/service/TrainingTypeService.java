package com.epam.gym.service;

import com.epam.gym.dto.response.TrainingTypeDTO;
import com.epam.gym.entity.TrainingType;
import com.epam.gym.entity.TrainingTypeEnum;
import com.epam.gym.mapper.TrainingTypeMapper;
import com.epam.gym.repository.TrainingTypeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TrainingTypeService {

    private final TrainingTypeRepository trainingTypeRepository;

    @Autowired
    public TrainingTypeService(TrainingTypeRepository trainingTypeRepository) {
        this.trainingTypeRepository = trainingTypeRepository;
    }

    public TrainingType getTrainingType(TrainingTypeEnum name) {
        return trainingTypeRepository.findByName(name).or(
                () -> Optional.of(new TrainingType(name))).get();
    }

    public List<TrainingTypeDTO> findAll() {
        return trainingTypeRepository.findAll().stream()
                .map(TrainingTypeMapper.mapToDTO)
                .toList();
    }

}
