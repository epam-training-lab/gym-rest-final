package com.epam.gym.entity;

import java.util.List;
import java.util.Set;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "trainer")
@Getter
@NoArgsConstructor
public class Trainer extends User {

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "specialization", nullable = false)
    private TrainingType specialization;

    @ManyToMany
    @JoinTable(
            name = "trainee2trainer",
            joinColumns = @JoinColumn(name = "trainer_id"),
            inverseJoinColumns = @JoinColumn(name = "trainee_id")
    )
    private Set<Trainee> trainees;

    @OneToMany(mappedBy = "trainer")
    private List<Training> trainings;

    public Trainer(String firstName, String lastName, String username, String password,
            boolean isActive, TrainingType specialization) {
        super(firstName, lastName, username, password, isActive);
        this.specialization = specialization;
    }

    public Trainer(Long userId, String firstName, String lastName, String username,
            String password, boolean isActive, TrainingType specialization,
            Set<Trainee> trainees, List<Training> trainings) {
        super(userId, firstName, lastName, username, password, isActive);
        this.specialization = specialization;
        this.trainees = trainees;
        this.trainings = trainings;
    }

}
