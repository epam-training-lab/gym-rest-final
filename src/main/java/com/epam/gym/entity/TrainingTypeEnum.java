package com.epam.gym.entity;

import lombok.Getter;

@Getter
public enum TrainingTypeEnum {
    FITNESS ("Fitness"),
    YOGA ("Yoga"),
    ZUMBA ("Zumba"),
    STRETCHING ("Stretching"),
    RESISTANCE ("Resistance");

    private final String name;

    TrainingTypeEnum(String name) {
        this.name = name;
    }

}
