package com.epam.gym.dto.request;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

public record SecureSetActiveDTO(
        @Valid @NotNull UserCredentialsDTO userCredentials,
        @NotNull Boolean isActive) {

}
