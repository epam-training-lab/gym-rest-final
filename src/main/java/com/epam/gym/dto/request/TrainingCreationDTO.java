package com.epam.gym.dto.request;

import com.epam.gym.entity.TrainingTypeEnum;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDate;

public record TrainingCreationDTO(
        @NotBlank String traineeUsername,
        @NotBlank String trainerUsername,
        @NotBlank String trainingName,
        @NotNull TrainingTypeEnum type,
        @NotNull LocalDate date,
        @NotNull Integer duration) {

}
