package com.epam.gym.dto.request;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

public record SecureTrainerUpdateDTO(
        @Valid @NotNull UserCredentialsDTO userCredentials,
        @Valid @NotNull TrainerUpdateDTO trainerUpdateInfo) {

}
