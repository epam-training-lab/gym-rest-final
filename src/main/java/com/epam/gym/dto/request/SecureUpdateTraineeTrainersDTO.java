package com.epam.gym.dto.request;

import java.util.List;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

public record SecureUpdateTraineeTrainersDTO(
        @Valid @NotNull UserCredentialsDTO userCredentials,
        @NotNull List<String> trainers) {

}
