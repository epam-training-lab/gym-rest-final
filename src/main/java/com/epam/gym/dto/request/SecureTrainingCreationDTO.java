package com.epam.gym.dto.request;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

public record SecureTrainingCreationDTO(
        @Valid @NotNull UserCredentialsDTO userCredentials,
        @Valid @NotNull TrainingCreationDTO training) {

}
