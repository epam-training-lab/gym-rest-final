package com.epam.gym.dto.request;

import jakarta.validation.constraints.NotBlank;

public record UserCredentialsDTO(
        @NotBlank String username,
        @NotBlank String password) {

}
