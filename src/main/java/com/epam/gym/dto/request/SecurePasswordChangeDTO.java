package com.epam.gym.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record SecurePasswordChangeDTO(
        @NotNull UserCredentialsDTO userCredentials,
        @NotBlank String newPassword) {

}
