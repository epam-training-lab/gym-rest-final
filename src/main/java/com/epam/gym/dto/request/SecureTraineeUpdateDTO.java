package com.epam.gym.dto.request;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

public record SecureTraineeUpdateDTO(
        @Valid @NotNull UserCredentialsDTO userCredentials,
        @Valid @NotNull TraineeUpdateDTO traineeUpdateInfo) {

}
