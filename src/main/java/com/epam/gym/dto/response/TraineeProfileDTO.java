package com.epam.gym.dto.response;

import java.time.LocalDate;
import java.util.List;

public record TraineeProfileDTO(
    String firstName,
    String lastName,
    LocalDate dateOfBirth,
    String address,
    boolean isActive,
    List<TrainerListItemDTO> trainers) {

}