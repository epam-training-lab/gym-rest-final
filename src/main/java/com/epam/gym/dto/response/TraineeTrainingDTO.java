package com.epam.gym.dto.response;

import com.epam.gym.entity.TrainingTypeEnum;

import java.time.LocalDate;

public record TraineeTrainingDTO(
        String name,
        LocalDate date,
        TrainingTypeEnum type,
        int duration,
        String trainerName) {

}
