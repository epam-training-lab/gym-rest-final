package com.epam.gym.dto.response;

public record TraineeListItemDTO(
        String username,
        String firstName,
        String lastName) {

}
