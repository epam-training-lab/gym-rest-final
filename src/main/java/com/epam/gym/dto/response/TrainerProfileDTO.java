package com.epam.gym.dto.response;

import com.epam.gym.entity.TrainingTypeEnum;

import java.util.List;

public record TrainerProfileDTO(
        String firstName,
        String lastName,
        TrainingTypeEnum specialization,
        boolean isActive,
        List<TraineeListItemDTO> trainees) {
}
