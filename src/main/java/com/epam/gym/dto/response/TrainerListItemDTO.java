package com.epam.gym.dto.response;

import com.epam.gym.entity.TrainingTypeEnum;

public record TrainerListItemDTO(
    String username,
    String firstName,
    String lastName,
    TrainingTypeEnum specialization) {
    
}
