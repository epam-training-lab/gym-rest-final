package com.epam.gym.controller;

import com.epam.gym.dto.request.SecureSetActiveDTO;
import com.epam.gym.dto.request.SecureTrainerUpdateDTO;
import com.epam.gym.dto.request.TrainerRegistrationDTO;
import com.epam.gym.dto.request.UserCredentialsDTO;
import com.epam.gym.dto.response.TrainerListItemDTO;
import com.epam.gym.dto.response.TrainerProfileDTO;
import com.epam.gym.dto.response.TrainerTrainingDTO;
import com.epam.gym.entity.TrainingTypeEnum;
import com.epam.gym.service.TrainerService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/trainers")
@Tag(name = "Trainer", description = "API for trainers")
public class TrainerController {

    private final TrainerService trainerService;

    @Autowired
    public TrainerController(TrainerService trainerService) {
        this.trainerService = trainerService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Register a new trainer")
    public UserCredentialsDTO registerTrainer(
            @Valid @RequestBody TrainerRegistrationDTO trainerRegistrationDTO) {
        return trainerService.registerTrainer(trainerRegistrationDTO);
    }

    @GetMapping("/{username}")
    @Operation(summary = "Get trainer profile by username")
    public TrainerProfileDTO getTrainerProfile(@PathVariable String username) {
        return trainerService.getTrainerProfile(username);
    }

    @PutMapping("/{username}")
    @Operation(summary = "Update trainer profile")
    public TrainerProfileDTO updateTrainerProfile(@PathVariable String username,
            @Valid @RequestBody SecureTrainerUpdateDTO secureTrainerUpdateDTO) {
        return trainerService.update(username, secureTrainerUpdateDTO);
    }

    @GetMapping("/non-assigned")
    @Operation(summary = "Get not assigned active trainers")
    public List<TrainerListItemDTO> getNotAssignedActiveTrainers() {
        return trainerService.getNotAssignActiveTrainers();
    }

    @GetMapping("/{username}/trainings")
    @Operation(summary = "Get trainings for a trainer")
    public List<TrainerTrainingDTO> getTrainings(
            @PathVariable String username,
            @RequestParam(required = false) LocalDate periodFrom,
            @RequestParam(required = false) LocalDate periodTo,
            @RequestParam(required = false) String traineeName,
            @RequestParam(required = false) TrainingTypeEnum type) {
        return trainerService.getTrainings(username, periodFrom, periodTo, traineeName, type);
    }

    @PatchMapping("{username}/active")
    @Operation(summary = "Set trainer active or inactive")
    public void setTrainerActive(@PathVariable String username,
            @Valid @RequestBody SecureSetActiveDTO secureSetActiveDTO) {
        trainerService.setActive(username, secureSetActiveDTO);
    }

}
