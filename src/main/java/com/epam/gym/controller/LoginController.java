package com.epam.gym.controller;

import com.epam.gym.dto.request.UserCredentialsDTO;
import com.epam.gym.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/login")
@Tag(name = "Login", description = "API for login")
public class LoginController {

    private final UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @Operation(summary = "Login a registered user")
    public void login(@Validated @RequestBody UserCredentialsDTO userCredentials) {
        userService.checkCredentials(userCredentials);
    }

}
