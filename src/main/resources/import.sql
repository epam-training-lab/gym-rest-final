-- Insert training types
INSERT INTO `training_type` (`name`) VALUES ('FITNESS'), ('YOGA'), ('ZUMBA'), ('STRETCHING'), ('RESISTANCE');

-- Insert app_users (3 trainers + 8 trainees = 11 users)
INSERT INTO `app_user` (`is_active`, `first_name`, `last_name`, `password`, `username`) VALUES (1, 'Jack', 'Black', 'password', 'jack.black'), (1, 'Jane', 'Smith', 'password', 'jane.smith'), (1, 'Jim', 'Taylor', 'password', 'jim.taylor'), (1, 'Alice', 'Johnson', 'password', 'alice.johnson'), (1, 'Bob', 'Brown', 'password', 'bob.brown'), (1, 'Charlie', 'Davis', 'password', 'charlie.davis'), (1, 'Diana', 'Evans', 'password', 'diana.evans'), (1, 'Emily', 'Wilson', 'password', 'emily.wilson'), (1, 'Frank', 'Thomas', 'password', 'frank.thomas'), (1, 'Grace', 'Martinez', 'password', 'grace.martinez'), (1, 'Henry', 'Anderson', 'password', 'henry.anderson');

-- Insert trainers with their specialization (assuming training_type IDs are 1 to 5)
INSERT INTO `trainer` (`id`, `specialization`) VALUES (1, 1), (2, 2), (3, 3);

-- Insert trainees
INSERT INTO `trainee` (`id`, `date_of_birth`, `address`) VALUES  (4, '1995-04-01', '123 Main St'), (5, '1996-05-02', '124 Main St'), (6, '1997-06-03', '125 Main St'), (7, '1998-07-04', '126 Main St'), (8, '1999-08-05', '127 Main St'), (9, '2000-09-06', '128 Main St'), (10, '2001-10-07', '129 Main St'), (11, '2002-11-08', '130 Main St');

-- Insert trainings (assuming dates and durations)
INSERT INTO `training` (`training_date`, `training_duration`, `trainee_id`, `trainer_id`, `training_type_id`, `training_name`) VALUES  ('2024-03-01', 60, 4, 1, 1, 'Morning Fitness'), ('2024-03-02', 45, 5, 2, 2, 'Yoga for Beginners'), ('2024-03-03', 30, 6, 3, 3, 'Zumba Dance'), ('2024-03-04', 60, 7, 1, 1, 'Advanced Fitness'), ('2024-03-05', 30, 8, 2, 2, 'Yoga Meditation'), ('2024-03-06', 45, 4, 2, 2, 'Evening Yoga'), ('2024-03-07', 50, 5, 3, 3, 'Morning Zumba'), ('2024-03-08', 60, 8, 1, 1, 'Fitness Challenge'), ('2024-03-09', 30, 8, 3, 3, 'Zumba Fun'), ('2024-03-10', 45, 8, 1, 4, 'Stretching Essentials'), ('2024-03-11', 50, 9, 2, 5, 'Resistance Training Basics');

-- Insert relationships into trainee2trainer
INSERT INTO `trainee2trainer` (`trainee_id`, `trainer_id`) VALUES (4, 1), (5, 2), (6, 3), (7, 1), (7, 2), (8, 1), (8, 2), (8, 3), (9, 1), (10, 3), (11, 1), (11, 3);
