package com.epam.gym.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import com.epam.gym.dto.request.*;
import com.epam.gym.dto.response.TraineeProfileDTO;
import com.epam.gym.dto.response.TraineeTrainingDTO;
import com.epam.gym.dto.response.TrainerListItemDTO;
import com.epam.gym.entity.*;
import com.epam.gym.mapper.TrainingFindMapper;
import com.epam.gym.repository.TrainerRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.epam.gym.repository.TraineeRepository;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

@SpringBootTest
@ActiveProfiles("test")
class TraineeServiceTest {

    @Autowired
    private TraineeService traineeService;

    @MockBean
    private TraineeRepository traineeRepository;

    @MockBean
    private TrainerRepository trainerRepository;

    @MockBean
    private UserService userService;

    @MockBean
    private TrainingService trainingService;

    private static Trainer trainer1;
    private static Trainer trainer2;
    private static Training training1;
    private static Training training2;
    private static Training training3;
    private static Trainee testTrainee;
    private UserCredentialsDTO userCredentials;

    @BeforeAll
    static void beforeAll() {

        testTrainee = new Trainee(
                1L,
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                LocalDate.now(),
                "address",
                new LinkedHashSet<>(),
                new ArrayList<>());

        trainer1 = new Trainer(
                10L,
                "Trainer1",
                "LastName",
                "trainer.1",
                "Password",
                true,
                new TrainingType(TrainingTypeEnum.FITNESS),
                new LinkedHashSet<>(),
                List.of());

        trainer2 = new Trainer(
                20L,
                "Trainer2",
                "LastName",
                "trainer.2",
                "Password",
                false,
                new TrainingType(TrainingTypeEnum.ZUMBA),
                new HashSet<>(),
                List.of());

        training1 = new Training(
                testTrainee,
                trainer1,
                "Training1",
                trainer1.getSpecialization(),
                LocalDate.of(2024, 1, 1),
                20);

        training2 = new Training(
                testTrainee,
                trainer2,
                "Training2",
                trainer2.getSpecialization(),
                LocalDate.of(2023, 12, 11),
                20);

        training3 = new Training(
                testTrainee,
                trainer2,
                "Training3",
                trainer2.getSpecialization(),
                LocalDate.of(2024, 2, 1),
                20);
    }

    @BeforeEach
    @SuppressWarnings("null")
    void setUp() {
        testTrainee = new Trainee(
                1L,
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                LocalDate.now(),
                "address",
                new HashSet<>(),
                new ArrayList<>());

        userCredentials = new UserCredentialsDTO(
                testTrainee.getUsername(),
                testTrainee.getPassword());

        when(traineeRepository.save(any(Trainee.class))).thenAnswer(invocation -> {
            Trainee trainee = invocation.getArgument(0);
            return new Trainee(
                    trainee.getUserId(),
                    trainee.getFirstName(),
                    trainee.getLastName(),
                    trainee.getUsername(),
                    trainee.getPassword(),
                    trainee.isActive(),
                    trainee.getDateOfBirth(),
                    trainee.getAddress(),
                    trainee.getTrainers(),
                    List.of());
        });

        when(traineeRepository.findByUsernameIgnoreCase(anyString())).thenReturn(Optional.of(testTrainee));
        when(userService.generatePassword()).thenReturn("password-generated");
        doNothing().when(userService).checkCredentials(any(UserCredentialsDTO.class));
    }

    @Test
    void registerNewTrainee() {
        when(userService.generateUsername(anyString(), anyString())).thenAnswer(invocation -> {
           String firstName = invocation.getArgument(0);
           String lastName = invocation.getArgument(1);
           return firstName.toLowerCase() + "." + lastName.toLowerCase();
        });

        var traineeCreationDTO = new TraineeRegistrationDTO(
                testTrainee.getFirstName(),
                testTrainee.getLastName(),
                testTrainee.getDateOfBirth(),
                testTrainee.getAddress());

        UserCredentialsDTO userCredentials = traineeService.registerTrainee(traineeCreationDTO);
        assertEquals("john.doe", userCredentials.username());
        assertEquals("password-generated", userCredentials.password());
    }

    @Test
    void updateExistentTrainee() {
        var traineeUpdateDTO = new TraineeUpdateDTO(
                "John",
                "Doe",
                LocalDate.now(),
                "New address",
                false);

        var expectedTraineeProfileDTO = new TraineeProfileDTO(
                traineeUpdateDTO.firstName(),
                traineeUpdateDTO.lastName(),
                traineeUpdateDTO.dateOfBirth(),
                traineeUpdateDTO.address(),
                traineeUpdateDTO.isActive(),
                List.of());

        var secTraineeDTO = new SecureTraineeUpdateDTO(userCredentials, traineeUpdateDTO);
        TraineeProfileDTO resultTraineeProfile = traineeService.update("john.doe", secTraineeDTO);
        assertEquals(expectedTraineeProfileDTO, resultTraineeProfile);
    }

    @Test
    void updateTrainers() {
        List<Trainer> trainerList = List.of(trainer1, trainer2);
        var secureUpdateTraineeTrainersDTO = new SecureUpdateTraineeTrainersDTO(userCredentials,
                List.of(trainer1.getUsername(), trainer2.getUsername()));

        List<TrainerListItemDTO> expected = Stream.of(trainer1, trainer2).map(trainer ->
                new TrainerListItemDTO(
                        trainer.getUsername(),
                        trainer.getFirstName(),
                        trainer.getLastName(),
                        trainer.getSpecialization().getName())).toList();

        when(trainerRepository.findByUsernameIgnoreCase(anyString())).thenAnswer(
                invocation -> {
                    String username = invocation.getArgument(0);
                    return Optional.of(trainerList.stream().filter(trainer ->
                            trainer.getUsername().equals(username)).findAny().orElseThrow());
                }
        );

        List<TrainerListItemDTO> result = traineeService.updateTrainers(
                testTrainee.getUsername(), secureUpdateTraineeTrainersDTO);

        assertIterableEquals(expected, result);
    }

    @Test
    void setActive() {
        doNothing().when(userService).setActive(any(User.class), anyBoolean());

        var secureActiveDTO = new SecureSetActiveDTO(userCredentials, false);
        traineeService.setActive(testTrainee.getUsername(), secureActiveDTO);
        verify(userService).setActive(testTrainee, false);

        secureActiveDTO = new SecureSetActiveDTO(userCredentials, true);
        traineeService.setActive(testTrainee.getUsername(), secureActiveDTO);
        verify(userService).setActive(testTrainee, true);
    }

    @Test
    void changeTraineePassword() {
        String newPassword = "new-password";

        doNothing().when(userService).changePassword(anyString(), any(SecurePasswordChangeDTO.class));
        when(traineeRepository.findByUsernameIgnoreCase(anyString())).thenReturn(Optional.of(
            new Trainee(
                    testTrainee.getUserId(),
                    testTrainee.getFirstName(),
                    testTrainee.getLastName(),
                    testTrainee.getUsername(),
                    newPassword,
                    testTrainee.isActive(),
                    testTrainee.getDateOfBirth(),
                    testTrainee.getAddress(),
                    testTrainee.getTrainers(),
                    List.of())
        ));

        Trainee trainee = traineeService.changePassword(
                testTrainee.getUsername(),
                new SecurePasswordChangeDTO(
                        new UserCredentialsDTO(
                                testTrainee.getUsername(),
                                testTrainee.getPassword()),
                        newPassword));

        assertEquals(newPassword, trainee.getPassword());
    }

    @Test
    @SuppressWarnings("null")
    void deleteTrainee() {
        traineeService.delete(
                new UserCredentialsDTO("username", "password"),
                testTrainee.getUsername());

        verify(traineeRepository).delete(testTrainee);
    }

    @Test
    void getTrainingsFromDate() {
        testTrainee.getTrainings().add(training1);
        testTrainee.getTrainings().add(training2);
        testTrainee.getTrainings().add(training3);

        List<TraineeTrainingDTO> expectedTrainings = Lists.list(training1, training3).stream()
                .map(TrainingFindMapper.mapTrainee).toList();

        List<TraineeTrainingDTO> resultTrainings = traineeService.getTrainings(
                testTrainee.getUsername(),
                LocalDate.of(2024, 1, 1),
                null, null, null);

        assertIterableEquals(expectedTrainings, resultTrainings);
    }

    @Test
    void getTrainingsToDate() {
        testTrainee.getTrainings().add(training1);
        testTrainee.getTrainings().add(training2);
        testTrainee.getTrainings().add(training3);

        List<TraineeTrainingDTO> expectedTrainings = Lists.list(training1, training2).stream()
                .map(TrainingFindMapper.mapTrainee).toList();

        List<TraineeTrainingDTO> resultTrainings = traineeService.getTrainings(
                testTrainee.getUsername(), null,
                LocalDate.of(2024, 1, 1), null, null);

        assertIterableEquals(expectedTrainings, resultTrainings);
    }

    @Test
    void getTrainingsByTrainerName() {
        testTrainee.getTrainings().add(training1);
        testTrainee.getTrainings().add(training2);
        testTrainee.getTrainings().add(training3);

        List<TraineeTrainingDTO> expectedTrainings = Lists.list(training2, training3).stream()
                .map(TrainingFindMapper.mapTrainee).toList();

        List<TraineeTrainingDTO> resultTrainings = traineeService.getTrainings(
                testTrainee.getUsername(), null, null,
                trainer2.getFirstName(), null);

        assertIterableEquals(expectedTrainings, resultTrainings);
    }

}