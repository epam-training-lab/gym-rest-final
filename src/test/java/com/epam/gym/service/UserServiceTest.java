package com.epam.gym.service;

import com.epam.gym.dto.request.UserCredentialsDTO;
import com.epam.gym.dto.request.SecurePasswordChangeDTO;
import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.User;
import com.epam.gym.exception.InvalidCredentialsException;
import com.epam.gym.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    private User testUser;

    @BeforeEach
    void setUp() {
        testUser = new Trainee(
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                null,
                null);
    }

    @Test
    void setActive() {
        userService.setActive(testUser, false);
        assertFalse(testUser.isActive());

        userService.setActive(testUser, true);
        assertTrue(testUser.isActive());
    }

    @Test
    @SuppressWarnings("null")
    void changePassword() {
        when(userRepository.existsByUsernameAndPassword(anyString(), anyString())).thenReturn(true);
        when(userRepository.findByUsernameIgnoreCase(anyString())).thenReturn(Optional.of(testUser));
        when(userRepository.save(any(User.class))).thenReturn(testUser);

        userService.changePassword(testUser.getUsername(), new SecurePasswordChangeDTO(
                new UserCredentialsDTO(testUser.getUsername(), testUser.getPassword()), "password"));
        verify(userRepository).changePassword(anyString(), anyString());
    }

    @Test
    void generateNonExistentUsername() {
        when(userRepository.countByUsernameStartingWith(anyString())).thenReturn(0);
        String expectedUsername = testUser.getFirstName().toLowerCase()
                + "." + testUser.getLastName().toLowerCase();
        String generatedUsername = userService.generateUsername(testUser.getFirstName(), testUser.getLastName());
        assertEquals(expectedUsername, generatedUsername);
    }

    @Test
    void generateExistentUsername() {
        when(userRepository.countByUsernameStartingWith(anyString())).thenReturn(3);
        String expectedUsername = testUser.getFirstName().toLowerCase()
                + "." + testUser.getLastName().toLowerCase() + "3";
        String generatedUsername = userService.generateUsername(testUser.getFirstName(), testUser.getLastName());
        assertEquals(expectedUsername, generatedUsername);
    }

    @Test
    void generatePassword() {
        String generatedPassword = userService.generatePassword();
        assertFalse(generatedPassword.isBlank());
    }

    @Test
    void checkValidCredentials() {
        when(userRepository.existsByUsernameAndPassword(anyString(), anyString())).thenReturn(true);
        assertDoesNotThrow(() -> userService.checkCredentials(
                new UserCredentialsDTO("username", "password")));
    }

    @Test
    void checkInvalidCredentials() {
        when(userRepository.existsByUsernameAndPassword(anyString(), anyString()))
                .thenReturn(false);
        assertThrows(InvalidCredentialsException.class,
                () -> userService.checkCredentials(
                        new UserCredentialsDTO("username", "password")));
    }

}