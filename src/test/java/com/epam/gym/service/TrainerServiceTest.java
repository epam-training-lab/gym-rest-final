package com.epam.gym.service;

import com.epam.gym.dto.request.*;
import com.epam.gym.dto.response.TrainerProfileDTO;
import com.epam.gym.dto.response.TrainerTrainingDTO;
import com.epam.gym.entity.*;
import com.epam.gym.mapper.TrainingFindMapper;
import com.epam.gym.repository.TrainerRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class TrainerServiceTest {

    @Autowired
    private TrainerService trainerService;

    @MockBean
    private TrainerRepository trainerRepository;

    @MockBean
    private UserService userService;

    @MockBean
    private TrainingService trainingService;

    private static Trainee trainee1;
    private static Trainee trainee2;
    private static Training training1;
    private static Training training2;
    private static Training training3;
    private static Trainer testTrainer;
    private UserCredentialsDTO userCredentials;

    @BeforeAll
    static void beforeAll() {

        testTrainer = new Trainer(
                1L,
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                new TrainingType(TrainingTypeEnum.FITNESS),
                Set.of(),
                new ArrayList<>());

        trainee1 = new Trainee(
                "Trainee1",
                "LastName",
                "Username",
                "Password",
                true,
                LocalDate.now(),
                "address");

        trainee2 = new Trainee(
                "Trainee2",
                "LastName",
                "Username",
                "Password",
                true,
                LocalDate.now(),
                "address");

        training1 = new Training(
                trainee1,
                testTrainer,
                "Training1",
                testTrainer.getSpecialization(),
                LocalDate.of(2024, 1, 1),
                20);

        training2 = new Training(
                trainee2,
                testTrainer,
                "Training2",
                testTrainer.getSpecialization(),
                LocalDate.of(2023, 12, 11),
                20);

        training3 = new Training(
                trainee2,
                testTrainer,
                "Training3",
                testTrainer.getSpecialization(),
                LocalDate.of(2024, 2, 1),
                20);
    }

    @BeforeEach
    @SuppressWarnings("null")
    void setUp() {
        testTrainer = new Trainer(
                1L,
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                new TrainingType(TrainingTypeEnum.FITNESS),
                Set.of(),
                new ArrayList<>());

        userCredentials = new UserCredentialsDTO(
                testTrainer.getUsername(),
                testTrainer.getPassword());

        when(trainerRepository.save(any(Trainer.class))).thenAnswer(invocation -> {
            Trainer trainer = invocation.getArgument(0);
            return new Trainer(
                    trainer.getUserId(),
                    trainer.getFirstName(),
                    trainer.getLastName(),
                    trainer.getUsername(),
                    trainer.getPassword(),
                    trainer.isActive(),
                    trainer.getSpecialization(),
                    trainer.getTrainees(),
                    trainer.getTrainings());
        });

        when(trainerRepository.findByUsernameIgnoreCase(anyString())).thenReturn(Optional.of(testTrainer));
        when(userService.generatePassword()).thenReturn("password-generated");
        doNothing().when(userService).checkCredentials(any(UserCredentialsDTO.class));
    }

    @Test
    void createNewTrainer() {
        when(userService.generateUsername(anyString(), anyString())).thenAnswer(invocation -> {
            String firstName = invocation.getArgument(0);
            String lastName = invocation.getArgument(1);
            return firstName.toLowerCase() + "." + lastName.toLowerCase();
        });

        var trainerCreationDTO = new TrainerRegistrationDTO(
                testTrainer.getFirstName(),
                testTrainer.getLastName(),
                testTrainer.getSpecialization().getName());

        UserCredentialsDTO userCredentials = trainerService.registerTrainer(trainerCreationDTO);
        assertEquals("john.doe", userCredentials.username());
        assertEquals("password-generated", userCredentials.password());
    }

    @Test
    void updateExistentTrainer() {
        var updatedTrainer = new TrainerUpdateDTO(
                "John",
                "Doe",
                TrainingTypeEnum.RESISTANCE,
                false);

        var trainerUpdateDTO = new TrainerProfileDTO(
                updatedTrainer.firstName(),
                updatedTrainer.lastName(),
                updatedTrainer.specialization(),
                updatedTrainer.isActive(),
                List.of());

        var secTrainerDTO = new SecureTrainerUpdateDTO(userCredentials, updatedTrainer);
        TrainerProfileDTO resultTrainerProfile = trainerService.update("john.doe", secTrainerDTO);
        assertEquals(trainerUpdateDTO, resultTrainerProfile);
    }

    @Test
    void toggleActive() {
        doNothing().when(userService).setActive(any(User.class), anyBoolean());

        var secureActiveDTO = new SecureSetActiveDTO(userCredentials, false);
        trainerService.setActive(testTrainer.getUsername(), secureActiveDTO);
        verify(userService).setActive(testTrainer, false);

        secureActiveDTO = new SecureSetActiveDTO(userCredentials, true);
        trainerService.setActive(testTrainer.getUsername(), secureActiveDTO);
        verify(userService).setActive(testTrainer, true);
    }

    @Test
    void changeTrainerPassword() {
        String newPassword = "new-password";

        doNothing().when(userService).changePassword(anyString(), any(SecurePasswordChangeDTO.class));
        when(trainerRepository.findByUsernameIgnoreCase(anyString())).thenReturn(Optional.of(
                new Trainer(
                        testTrainer.getUserId(),
                        testTrainer.getFirstName(),
                        testTrainer.getLastName(),
                        testTrainer.getUsername(),
                        newPassword,
                        testTrainer.isActive(),
                        testTrainer.getSpecialization(),
                        testTrainer.getTrainees(),
                        testTrainer.getTrainings())
        ));

        Trainer trainer = trainerService.changePassword(
                testTrainer.getUsername(),
                new SecurePasswordChangeDTO(
                        new UserCredentialsDTO(
                                testTrainer.getUsername(),
                                testTrainer.getPassword()),
                        newPassword));

        assertEquals(newPassword, trainer.getPassword());
    }

    @Test
    void getTrainingsFromDate() {
        testTrainer.getTrainings().add(training1);
        testTrainer.getTrainings().add(training2);
        testTrainer.getTrainings().add(training3);

        List<TrainerTrainingDTO> expectedTrainings = Lists.list(training1, training3).stream()
                        .map(TrainingFindMapper.mapTrainer).toList();

        List<TrainerTrainingDTO> resultTrainings = trainerService.getTrainings(
                testTrainer.getUsername(), LocalDate.of(2024, 1, 1),
                null, null, null);

        assertIterableEquals(expectedTrainings, resultTrainings);
    }

    @Test
    void getTrainingsToDate() {
        testTrainer.getTrainings().add(training1);
        testTrainer.getTrainings().add(training2);
        testTrainer.getTrainings().add(training3);

        List<TrainerTrainingDTO> expectedTrainings = Lists.list(training1, training2).stream()
                        .map(TrainingFindMapper.mapTrainer).toList();

        List<TrainerTrainingDTO> resultTrainings = trainerService.getTrainings(
                testTrainer.getUsername(), null,
                LocalDate.of(2024, 1, 1), null ,null);

        assertIterableEquals(expectedTrainings, resultTrainings);
    }

    @Test
    void getTrainingsByTrainerName() {
        testTrainer.getTrainings().add(training1);
        testTrainer.getTrainings().add(training2);
        testTrainer.getTrainings().add(training3);

        List<TrainerTrainingDTO> expectedTrainings = Lists.list(training2, training3).stream()
                        .map(TrainingFindMapper.mapTrainer).toList();
                        
        List<TrainerTrainingDTO> resultTrainings = trainerService.getTrainings(
                testTrainer.getUsername(), null, null,
                trainee2.getFirstName(), null);

        assertIterableEquals(expectedTrainings, resultTrainings);
    }

}