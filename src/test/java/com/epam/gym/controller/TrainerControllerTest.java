package com.epam.gym.controller;

import com.epam.gym.dto.request.*;
import com.epam.gym.dto.response.TrainerProfileDTO;
import com.epam.gym.entity.TrainingTypeEnum;
import com.epam.gym.exception.EntityNotFoundException;
import com.epam.gym.service.TrainerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(TrainerController.class)
class TrainerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TrainerService trainerService;

    private static ObjectMapper objectMapper;
    private static UserCredentialsDTO userCredentials;
    private static TrainerProfileDTO trainerProfileDTO;

    @BeforeAll
    static void setup() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        userCredentials = new UserCredentialsDTO("john.doe", "password");

        trainerProfileDTO = new TrainerProfileDTO(
                "John",
                "Doe",
                TrainingTypeEnum.RESISTANCE,
                true,
                List.of());
    }

    @Test
    @SuppressWarnings("null")
    void registerTrainerValidCredentials() throws Exception {
        var trainerRegistrationDTO = new TrainerRegistrationDTO(
                trainerProfileDTO.firstName(),
                trainerProfileDTO.lastName(),
                trainerProfileDTO.specialization());

        when(trainerService.registerTrainer(any(TrainerRegistrationDTO.class))).thenAnswer(
                invocation -> {
                    TrainerRegistrationDTO trainerInfo = invocation.getArgument(0);
                    return new UserCredentialsDTO(
                            trainerInfo.firstName().toLowerCase()
                                    + "." + trainerInfo.lastName().toLowerCase(),
                            "password");
                });

        mockMvc.perform(post("/api/trainers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(trainerRegistrationDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.username").value("john.doe"))
                .andExpect(jsonPath("$.password").value("password"));
    }

    @Test
    void getExistentTrainerProfile() throws Exception {
        when(trainerService.getTrainerProfile(anyString())).thenReturn(trainerProfileDTO);
        mockMvc.perform(get("/api/trainers/" + userCredentials.username()))
                .andExpect(status().isOk());

        verify(trainerService).getTrainerProfile(userCredentials.username());
    }

    @Test
    void getNonExistentTrainerProfile() throws Exception {
        doThrow(EntityNotFoundException.class).when(trainerService).getTrainerProfile(anyString());
        mockMvc.perform(get("/api/trainers/" + userCredentials.username()))
                .andExpect(status().isNotFound());

        verify(trainerService).getTrainerProfile(userCredentials.username());
    }

    @Test
    @SuppressWarnings("null")
    void updateExistentTrainerProfile() throws Exception {
        var trainerUpdateDTO = new TrainerUpdateDTO(
                trainerProfileDTO.firstName(),
                trainerProfileDTO.lastName(),
                trainerProfileDTO.specialization(),
                trainerProfileDTO.isActive());

        var secureTrainerUpdateDTO = new SecureTrainerUpdateDTO(userCredentials, trainerUpdateDTO);

        when(trainerService.update(anyString(), any(SecureTrainerUpdateDTO.class))).thenReturn(trainerProfileDTO);
        mockMvc.perform(put("/api/trainers/" + userCredentials.username())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(secureTrainerUpdateDTO)))
                .andExpect(status().isOk());

        verify(trainerService).update(userCredentials.username(), secureTrainerUpdateDTO);
    }

    @Test
    void getExistentTrainerTrainings() throws Exception {
        when(trainerService.getTrainings(any(), any(), any(), any(), any())).thenReturn(List.of());
        mockMvc.perform(get("/api/trainers/" + userCredentials.username() + "/trainings"))
                .andExpect(status().isOk());

        verify(trainerService).getTrainings(
                userCredentials.username(),
                null,
                null,
                null,
                null);
    }

    @Test
    @SuppressWarnings("null")
    void setExistentTrainerActive() throws Exception {
        var secureSetActiveDTO = new SecureSetActiveDTO(userCredentials, false);

        doNothing().when(trainerService).setActive(userCredentials.username(), secureSetActiveDTO);
        mockMvc.perform(patch("/api/trainers/" + userCredentials.username() + "/active")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(secureSetActiveDTO)))
                .andExpect(status().isOk());

        verify(trainerService).setActive(userCredentials.username(), secureSetActiveDTO);
    }

    @Test
    void getNotAssignedTrainers() throws Exception {
        when(trainerService.getNotAssignActiveTrainers()).thenReturn(List.of());
        mockMvc.perform(get("/api/trainers/non-assigned"))
                .andExpect(status().isOk());

        verify(trainerService).getNotAssignActiveTrainers();
    }

}