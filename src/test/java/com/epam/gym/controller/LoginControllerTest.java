package com.epam.gym.controller;

import com.epam.gym.dto.request.UserCredentialsDTO;
import com.epam.gym.exception.InvalidCredentialsException;
import com.epam.gym.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(LoginController.class)
class LoginControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    private static ObjectMapper objectMapper;

    @BeforeAll
    static void setup() {
        objectMapper = new ObjectMapper();
    }

    @Test
    @SuppressWarnings("null")
    void loginWithValidCredentials() throws Exception {
        var userCredentials = new UserCredentialsDTO("john.doe", "password");

        doNothing().when(userService).checkCredentials(any(UserCredentialsDTO.class));
        mockMvc.perform(get("/api/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userCredentials)))
                .andExpect(status().isOk());
    }

    @Test
    @SuppressWarnings("null")
    void loginWithInvalidCredentials() throws Exception {
        var userCredentials = new UserCredentialsDTO("john.doe", "password");

        doThrow(InvalidCredentialsException.class).
                when(userService).checkCredentials(any(UserCredentialsDTO.class));
        mockMvc.perform(get("/api/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userCredentials)))
                .andExpect(status().isUnauthorized());
    }

}