package com.epam.gym.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gym.dto.request.SecurePasswordChangeDTO;
import com.epam.gym.dto.request.UserCredentialsDTO;
import com.epam.gym.exception.InvalidCredentialsException;
import com.epam.gym.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

@ActiveProfiles("test")
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    private UserService userService;

    private static ObjectMapper objectMapper;

    @BeforeAll
    static void beforeClass() {
        objectMapper = new ObjectMapper();
    }

    @Test
    @SuppressWarnings("null")
    void changePasswordValidCredentials() throws Exception {
        var userCredentials = new UserCredentialsDTO("john.doe", "password");
        var securePasswordChangeDTO = new SecurePasswordChangeDTO(
                userCredentials, "new-password");

        doNothing().when(userService).checkCredentials(any(UserCredentialsDTO.class));
        doNothing().when(userService).changePassword(anyString(), any(SecurePasswordChangeDTO.class));
        mockMvc.perform(put("/api/users/" + userCredentials.username() + "/password-change")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(securePasswordChangeDTO)))
                .andExpect(status().isOk());
    }

    @Test
    @SuppressWarnings("null")
    void changePasswordInvalidCredentials() throws Exception {
        var userCredentials = new UserCredentialsDTO("john.doe", "password");
        var securePasswordChangeDTO = new SecurePasswordChangeDTO(
                userCredentials, "new-password");

        doThrow(InvalidCredentialsException.class)
                .when(userService).changePassword(anyString(), any(SecurePasswordChangeDTO.class));
        mockMvc.perform(put("/api/users/" + userCredentials.username() + "/password-change")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(securePasswordChangeDTO)))
                .andExpect(status().isUnauthorized());
    }

}
