package com.epam.gym.controller;

import com.epam.gym.dto.request.SecureTrainingCreationDTO;
import com.epam.gym.dto.request.TrainingCreationDTO;
import com.epam.gym.dto.request.UserCredentialsDTO;
import com.epam.gym.entity.TrainingTypeEnum;
import com.epam.gym.service.TrainingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(TrainingController.class)
class TrainingControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TrainingService trainingService;

    private static ObjectMapper objectMapper;
    private static UserCredentialsDTO userCredentials;

    @BeforeAll
    static void beforeAll() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        userCredentials = new UserCredentialsDTO("john.doe", "password");
    }

    @Test
    @SuppressWarnings("null")
    void createTraining() throws Exception {
        var trainingCreationDTO = new TrainingCreationDTO(
                "trainee.username",
                "trainer.username",
                "Training name",
                TrainingTypeEnum.FITNESS,
                LocalDate.now(),
                10);

        var secureTrainingCreationDTO = new SecureTrainingCreationDTO(userCredentials, trainingCreationDTO);

        doNothing().when(trainingService).create(secureTrainingCreationDTO);
        mockMvc.perform(post("/api/trainings")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(secureTrainingCreationDTO)))
                .andExpect(status().isOk());

        verify(trainingService).create(secureTrainingCreationDTO);
    }

}