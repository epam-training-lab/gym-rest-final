package com.epam.gym.controller;

import com.epam.gym.dto.request.*;
import com.epam.gym.dto.response.TraineeProfileDTO;
import com.epam.gym.exception.EntityNotFoundException;
import com.epam.gym.service.TraineeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(TraineeController.class)
class TraineeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TraineeService traineeService;

    private static ObjectMapper objectMapper;
    private static UserCredentialsDTO userCredentials;
    private static TraineeProfileDTO traineeProfileDTO;

    @BeforeAll
    static void setup() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        userCredentials = new UserCredentialsDTO("john.doe", "password");

        traineeProfileDTO = new TraineeProfileDTO(
                "John",
                "Doe",
                LocalDate.now(),
                "Address",
                true,
                List.of());
    }

    @Test
    @SuppressWarnings("null")
    void registerTraineeValidCredentials() throws Exception {
        var traineeRegistrationDTO = new TraineeRegistrationDTO(
                traineeProfileDTO.firstName(),
                traineeProfileDTO.lastName(),
                traineeProfileDTO.dateOfBirth(),
                traineeProfileDTO.address());

        when(traineeService.registerTrainee(any(TraineeRegistrationDTO.class))).thenAnswer(
                invocation -> {
                    TraineeRegistrationDTO traineeInfo = invocation.getArgument(0);
                    return new UserCredentialsDTO(
                            traineeInfo.firstName().toLowerCase()
                                    + "." + traineeInfo.lastName().toLowerCase(),
                            "password");
                });

        mockMvc.perform(post("/api/trainees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(traineeRegistrationDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.username").value("john.doe"))
                .andExpect(jsonPath("$.password").value("password"));
    }

    @Test
    @SuppressWarnings("null")
    void exceptionHandlingInvalidFieldValues() throws Exception {
        var traineeRegistrationDTO = new TraineeRegistrationDTO(
                traineeProfileDTO.firstName(),
                null,
                null,
                null);

        when(traineeService.registerTrainee(any(TraineeRegistrationDTO.class))).thenReturn(userCredentials);
        mockMvc.perform(post("/api/trainees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(traineeRegistrationDTO)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getExistentTraineeProfile() throws Exception {
        when(traineeService.getTraineeProfile(anyString())).thenReturn(traineeProfileDTO);
        mockMvc.perform(get("/api/trainees/" + userCredentials.username()))
                .andExpect(status().isOk());

        verify(traineeService).getTraineeProfile(userCredentials.username());
    }

    @Test
    void getNonExistentTraineeProfile() throws Exception {
        doThrow(EntityNotFoundException.class).when(traineeService).getTraineeProfile(anyString());
        mockMvc.perform(get("/api/trainees/" + userCredentials.username()))
                .andExpect(status().isNotFound());

        verify(traineeService).getTraineeProfile(userCredentials.username());
    }

    @Test
    @SuppressWarnings("null")
    void updateExistentTraineeProfile() throws Exception {
        var traineeUpdateDTO = new TraineeUpdateDTO(
                "John",
                "Doe",
                LocalDate.now(),
                "Address",
                true);

        var secureTraineeUpdateDTO = new SecureTraineeUpdateDTO(userCredentials, traineeUpdateDTO);

        when(traineeService.update(anyString(), any(SecureTraineeUpdateDTO.class))).thenReturn(traineeProfileDTO);
        mockMvc.perform(put("/api/trainees/" + userCredentials.username())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(secureTraineeUpdateDTO)))
                .andExpect(status().isOk());

        verify(traineeService).update(userCredentials.username(), secureTraineeUpdateDTO);
    }

    @Test
    @SuppressWarnings("null")
    void deleteExistentTraineeProfile() throws Exception {
        doNothing().when(traineeService).delete(any(UserCredentialsDTO.class), anyString());
        mockMvc.perform(delete("/api/trainees/" + userCredentials.username())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userCredentials)))
                .andExpect(status().isOk());

        verify(traineeService).delete(userCredentials, userCredentials.username());
    }

    @Test
    @SuppressWarnings("null")
    void updateExistentTraineeTrainers() throws Exception {
        var secureUpdateTraineeTrainersDTO = new SecureUpdateTraineeTrainersDTO(userCredentials, List.of());

        when(traineeService.updateTrainers(anyString(), any(SecureUpdateTraineeTrainersDTO.class)))
                .thenReturn(List.of());
        mockMvc.perform(put("/api/trainees/" + userCredentials.username() + "/trainers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(secureUpdateTraineeTrainersDTO)))
                .andExpect(status().isOk());

        verify(traineeService).updateTrainers(userCredentials.username(), secureUpdateTraineeTrainersDTO);
    }

    @Test
    void getExistentTraineeTrainings() throws Exception {
        when(traineeService.getTrainings(any(), any(), any(), any(), any())).thenReturn(List.of());
        mockMvc.perform(get("/api/trainees/" + userCredentials.username() + "/trainings"))
                .andExpect(status().isOk());

        verify(traineeService).getTrainings(
                userCredentials.username(),
                null,
                null,
                null,
                null);
    }

    @Test
    @SuppressWarnings("null")
    void setExistentTraineeActive() throws Exception {
        var secureSetActiveDTO = new SecureSetActiveDTO(userCredentials, false);

        doNothing().when(traineeService).setActive(userCredentials.username(), secureSetActiveDTO);
        mockMvc.perform(patch("/api/trainees/" + userCredentials.username() + "/active")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(secureSetActiveDTO)))
                .andExpect(status().isOk());

        verify(traineeService).setActive(userCredentials.username(), secureSetActiveDTO);
    }

}