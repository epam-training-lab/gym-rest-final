package com.epam.gym.repository;

import com.epam.gym.entity.Trainee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@ActiveProfiles("test")
class TraineeRepositoryTest {

    @Autowired
    private TraineeRepository traineeRepository;
    private Trainee testTrainee;

    @BeforeEach
    void setUp() {
        testTrainee = new Trainee(
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                LocalDate.now(),
                "address");

        traineeRepository.save(testTrainee);
    }

    @Test
    void findTraineeByUsername() {
        Trainee trainee = traineeRepository.findByUsernameIgnoreCase(testTrainee.getUsername()).orElseThrow();
        assertEquals(testTrainee.getUsername(), trainee.getUsername());
    }

}