package com.epam.gym.repository;

import com.epam.gym.entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ActiveProfiles("test")
class TrainerRepositoryTest {

    @Autowired
    private TrainerRepository trainerRepository;
    private Trainer testTrainer;

    @BeforeEach
    void setUp() {
        this.testTrainer = new Trainer(
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                new TrainingType(TrainingTypeEnum.FITNESS));

        trainerRepository.save(testTrainer);
    }

    @Test
    void findTrainerByUsername() {
        Trainer trainer = trainerRepository.findByUsernameIgnoreCase(testTrainer.getUsername()).orElseThrow();
        assertEquals(testTrainer.getUsername(), trainer.getUsername());
    }

}